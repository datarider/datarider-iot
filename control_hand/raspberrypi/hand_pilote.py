import time
import serial
from adafruit_servokit import ServoKit
from datetime import datetime

# ===============================================================
#           INIT
# ===============================================================
# Set channels to the number of servo channels on your kit.
# 8 for FeatherWing, 16 for Shield/HAT/Bonnet.
# docs: https://docs.circuitpython.org/projects/servokit/en/latest/
kit = ServoKit(channels=16)

# Initialisation du port série
ser = serial.Serial("/dev/ttyUSB0", 115200)

HAND_MAX_ANGLE = 120
HAND_MIN_ANGLE = 10


# ===============================================================
#           LOW LEVEL FUNCTION
# ===============================================================


def log(level, str):
    print(datetime.now(), "[" + level.upper() + "] " + str)


def logInfo(str):
    log("info", str)


def logDebug(str):
    log("debug", str)


def logError(str):
    log("error", str)


# Move servo by angle
def servo_change_angle(num, angle):
    kit.servo[num].angle = angle
    logDebug("Servo " + str(num) + " moved to " + str(angle) + "°")


# ===============================================================
#           MIDDLE LEVEL FUNCTION
# ===============================================================


# Move thumb between autorized angle
def hand_thumb_move(angle):
    # Control angle value
    if angle > HAND_MAX_ANGLE:
        angle = HAND_MAX_ANGLE
    elif angle < HAND_MIN_ANGLE:
        angle = HAND_MIN_ANGLE
    # Apply change
    servo_change_angle(0, angle)
    logInfo("Thumb moved to " + str(angle) + "°")


def hand_thumb_unpress():
    hand_thumb_move(HAND_MAX_ANGLE)


def hand_thumb_press_level1():
    hand_thumb_move(70)


def hand_thumb_press_level2():
    hand_thumb_move(50)


def hand_thumb_press_level3():
    hand_thumb_move(35)


def hand_thumb_press_level4():
    hand_thumb_move(HAND_MIN_ANGLE)


# Move index
def hand_index_press():
    servo_change_angle(1, 110)
    logInfo("Index press")


def hand_index_unpress():
    servo_change_angle(1, 90)
    logInfo("Index unpress")


# ===============================================================
#           HIGH LEVEL FUNCTION
# ===============================================================


def hand_manage_kart(speed):
    logInfo("************************************************")
    logInfo("Kart speed set to " + str(speed))
    if speed <= 0 or speed > 5:
        hand_index_unpress()
        hand_thumb_unpress()
    elif speed == 1:
        hand_index_unpress()
        hand_thumb_press_level1()
    elif speed == 2:
        hand_index_unpress()
        hand_thumb_press_level2()
    elif speed == 3:
        hand_index_unpress()
        hand_thumb_press_level3()
    elif speed == 4:
        hand_index_unpress()
        hand_thumb_press_level4()
    elif speed == 5:
        hand_thumb_press_level4()
        hand_index_press()
    logInfo("************************************************")


# ===============================================================
#           MAIN
# ===============================================================

hand_manage_kart(0)
time.sleep(3)

try:
    while True:
        if ser.in_waiting > 0:
            command = ser.readline().decode("utf-8").strip()
            logInfo("Received command " + command)
            if len(str(command)) == 7:
                parts = command.split()
                if parts[0] == "speed":
                    hand_manage_kart(int(parts[1]))
            else:
                logDebug("Invalid command")

except Exception as inst:
    logError(f"An exception occurred : {inst}")
except KeyboardInterrupt:
    logError("An exception occurred : Keyboard Interruption of this program")


# Stop speed (release all fingers)
hand_manage_kart(0)
