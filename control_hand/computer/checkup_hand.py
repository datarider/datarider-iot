import serial
import time
from datetime import datetime

#########################################################################
#####           INIT                                                #####
#########################################################################

# Initialisation du port série
SERIAL_PORT = "/dev/ttyUSB0"  # Raspberry Pi 3
ser = serial.Serial(SERIAL_PORT, baudrate=115200, timeout=5)


#########################################################################
#####           LOW LEVEL FUNCTION                                  #####
#########################################################################


def log(level, str):
    print(datetime.now(), "[" + level.upper() + "] " + str)


def logInfo(str):
    log("info", str)


def send_command(command):
    logInfo(f"Command send '{command}'")
    ser.write(f"{command}\r\n".encode("utf-8"))


#########################################################################
#####           MIDDLE LEVEL FUNCTION                               #####
#########################################################################

# Apply speed to the hand
def apply_speed(speed, duration=3):
    logInfo(f"Apply SPEED = {speed} for {duration}s")
    send_command(f"speed {speed}")
    time.sleep(duration)


#########################################################################
#####           MAIN                                                #####
#########################################################################
if __name__ == "__main__":

    logInfo("Let's start the hand checkup !")
    time.sleep(1)
    # Init - Speed = 0
    apply_speed(0)
    # Change speed from 1 to 5
    apply_speed(1)
    apply_speed(2)
    apply_speed(3)
    apply_speed(4)
    apply_speed(5)
    # End - Speed = 0
    apply_speed(0)
    logInfo("End of the hand checkup")
