import serial
from datetime import datetime


# ===============================================================
#           INIT
# ===============================================================

# Initialisation du port série
SERIAL_PORT = "/dev/ttyUSB0"  # Raspberry Pi 3
ser = serial.Serial(SERIAL_PORT, baudrate=115200, timeout=5)


def send_command(command):
    ser.write(f"{command}\n".encode("utf-8"))
    print(datetime.now(), "[DEBUG] command = %s" % (command))


# ===============================================================
#           MAIN
# ===============================================================
if __name__ == "__main__":

    # Read input from user
    trk_n1 = "1"
    trk_n2 = "2"
    trk_default = trk_n2
    trk_num = input(f"Track [{trk_n1}|{trk_n2}] (default = {trk_default}):")
    if trk_num == trk_n1 or trk_num == trk_n2:
        track_num_validate = trk_num
    else:
        track_num_validate = trk_default
    print("You entered: " + trk_num)
    print("Track num used: %s" % (track_num_validate))

    # Poll for new messages
    while True:
        speed_input = input("Speed [0-5] (default = 0): ")
        speed = int(speed_input)
        print("SPEED = %s" % (speed))
        send_command("speed %s\r" % (speed))
