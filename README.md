# Data Rider - IOT
Improve Mario Kart with IOT and AI

## Description

The IoT part is used to collect data on the car circuit (using an Arduino board).  
We can also use a Raspberry Pi board to control a mechanical hand activating one of the two joysticks.

## Data Interface Contract

In this V1, for each track, we have:

- A turn counter
- The gate number in front of which the car is located  (or default value if not located in front of any gate)
- The gate number of the last gate passed
- The negative potential (the difference between the known positive potential  
  and the measured negative potential will later allow the calculation of the voltage through the car).

For more details, read on [INTERFACE-CONTRACT.md](INTERFACE-CONTRACT.md)

## Getting started

### Power on the circuit
- Plug the socket into the power strip.

### Start the Raspberry Pi

- Enter the login and password  
- Then move to the following folder: ```cd serial_usb```  
- And run the following for the Raspberry Pi to be listening : ```python hand_pilote.py```
- 
### Init the Raspberry Pi 5
- Go to the home folder : `/home/datarider/`
- Copy the file `control_hand/raspberrypi/hand_pilote.py` inside the home folder

#### Activate script at start

##### Solution 1 : via services (need to be connected)
- Go to the system folder : `/etc/systemd/system`
- Copy the file `control_hand/raspberrypi/hand_pilote.service` inside the folder
- Start the servie : `sudo systemctl enable hand_pilote.service`

##### Solution 2 : via cron (recommanded for our usage)
```
crontab -e
@reboot /usr/bin/python3 /home/datarider/hand_pilote.py
```


### Set up the computer:

- Ensure that:  
    1) The white USB cable (Arduino) is connected  
    2) The USB cable with colored wires (Raspberry Pi) is connected  

- In the **datarider-etl** project: run ```.\init.sh```  
- In the **datarider-iot** project: run ```.\init.sh```. This starts the Kafka stream (receiving from the Arduino).  
- In the **datarider-ia** project: remember to activate the venv before running your scripts: ```source venv/bin/activate```

### At the end:

- Stop the Raspberry Pi scripts (ctrl + C)   
- Stop the Kafka script on the computer (ctrl + C in datarider-iot)  
- Unplug the circuit.


## Overview of electronic devices

In this V1, we have the following devices :

###  Car circuit

The two tracks are of equal length. Each is controlled by a joystick.

### Sensors

- Two voltage sensors (negative potential) are behind the circuit. There is one for each of the two tracks.  

- Hall effect position sensors are located at 8 points (for each track) on the circuit and indicate the car's location. See below their positions :

<table>
  <tr>
    <td>
      <table border="1">
        <caption>Track 1</caption>
        <tr>
          <th>Arduino</th>
          <th>Code</th>
          <th>Description</th>
        </tr>
        <tr>
          <td>PIN 22</td>
          <td>c1d</td>
          <td>exterior track / starting position</td>
        </tr>
        <tr>
          <td>PIN 23</td>
          <td>c1v1e</td>
          <td>exterior track / turn 1 / entry</td>
        </tr>
        <tr>
          <td>PIN 24</td>
          <td>c1v1s</td>
          <td>exterior track / turn 1 / exit</td>
        </tr>
        <tr>
          <td>PIN 25</td>
          <td>c1v2e</td>
          <td>exterior track / turn 2 / entry</td>
        </tr>
        <tr>
          <td>PIN 26</td>
          <td>c1v3s</td>
          <td>exterior track / turn 3 / exit</td>
        </tr>
        <tr>
          <td>PIN 27</td>
          <td>c1le</td>
          <td>exterior track / position looping / entry</td>
        </tr>
        <tr>
          <td>PIN 28</td>
          <td>c1ls</td>
          <td>exterior track / position looping / exit</td>
        </tr>
        <tr>
          <td>PIN 29</td>
          <td>c1v4e</td>
          <td>exterior track / turn 4 / entry</td>
        </tr>
      </table>
    </td>
    <td>
      <table border="1">
        <caption>Track 2</caption>
        <tr>
          <th>Arduino</th>
          <th>Code</th>
          <th>Description</th>
        </tr>
        <tr>
          <td>PIN 37</td>
          <td>c2d</td>
          <td>inside track / starting position</td>
        </tr>
        <tr>
          <td>PIN 36</td>
          <td>c2v1e</td>
          <td>inside track / turn 1 / entry</td>
        </tr>
        <tr>
          <td>PIN 35</td>
          <td>c2v1s</td>
          <td>inside track / turn 1 / exit</td>
        </tr>
        <tr>
          <td>PIN 34</td>
          <td>c2v2e</td>
          <td>inside track / turn 2 / entry</td>
        </tr>
        <tr>
          <td>PIN 33</td>
          <td>c2v3s</td>
          <td>inside track / turn 3 / exit</td>
        </tr>
        <tr>
          <td>PIN 32</td>
          <td>c2le</td>
          <td>inside track / position looping / entry</td>
        </tr>
        <tr>
          <td>PIN 31</td>
          <td>c2ls</td>
          <td>inside track / position looping / exit</td>
        </tr>
        <tr>
          <td>PIN 30</td>
          <td>c2v4e</td>
          <td>inside track / turn 4 / entry</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

![Track with sensor position](/ressources/media/gates_positions.png)


### Arduino board
This is an electronic board located under the circuit (under the table). It is used to collect physical data. 
The code running on the Arduino is sent via USB to the computer.  
The Arduino code is located in the "collect" folder.  
If you want to modify and upload changes to the Arduino, you need to use the specific Arduino IDE. 
Once the code is uploaded to the Arduino, it remains persistent: there is no need to reload it every time.

### USB cable
The USB cable (white cable) is used to send the data collected by the Arduino to the computer.

### Computer
To receive and process the data.

### Python reception script
On the computer, a Python script reads the data arriving on the computer's USB port, 
filters out incorrect lines, and outputs the data into a Kafka stream.
The subsequent data processing (ETL and beyond) reads this Kafka data stream.

### Raspberry Pi
The Raspberry Pi is an electronic card used to control the mechanical hand based on instructions given by the computer.  
To interact with the Raspberry Pi:
use an HDMI cable for a display and one of the USB ports for a keyboard.
As soon as the RP is powered on, it prompts for a login and password: they are displayed on the wall next to the circuit.
The Raspberry Pi communicates via USB to USB with the computer.

### Mechanical Hand
It is used to control a remote.
The AI part of the project aims to develop a learning algorithm to improve the car driving performance of the mechanical hand.

## Summarize of the data "journey":

```mermaid
graph TD
I[Raspberry Pi activates the mechanical hand : the IA presses the joystick] --> O[The little car is moving around the track.]
O --> A[Physical data production on the circuit]
A --> B[Collection by the Arduino]
B --> C[Transfer via USB to the computer]
C --> D[USB port reading by Python script]
D --> E[Sending data into a Kafka stream]
E --> F[Data processing]
F --> G[Possible sending of instructions to the Raspberry Pi]
G --> H[Python script sends instructions via another USB port to the Raspberry Pi]
H --> I
```


## Setup

### Setup Arduino
1. Download and install Arduino IDE  
2. In the library section find `ACS712` - More on [Github ACS712](https://github.com/RobTillaart/ACS712)  
3. Open the file `collect/sketch_Circuit_Acquisition_gates_voltage/sketch_Circuit_Acquisition_gates_voltage.ino`  
4. Link Arduino to external power  
5. Connect Arduino to your computer (the number of sensors may cause USB killing)  
6. Push the code to the Arduino 

Remember to set the baud rate (the same as the one in the Arduino code) in the Arduino IDE if you want to view the data live.

### Setup python venv
### Create venv
    python3 -m venv venv

#### Activate venv
    source venv/bin/activate

#### Install libraries
    pip install -r requirements.txt

#### Stop venv
    desactivate

### Setup Python2Kafka
    source venv/bin/activate
    python3 expose/python2kafka/csv_report_kafka.py


## Contributing
Orange Business and Business&Decision members only

## Authors and acknowledgment
For this current repository, sorted by alphabetical name:
- DEZERE David as electronic & arduino developper
- GARRY Freddy as electronic & arduino developper
- LE PENMELEN Cédric as product owner
- ROY Jean-Christophe as electronic & arduino developper
- CHIAPELLO Juliette as developper

## License
X11 License Distribution Modification Variant - See LICENSE file