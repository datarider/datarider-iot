from datetime import datetime


def log(level, str):
    print(datetime.now(), " [" + level.upper() + "] " + str)


def logDebug(str):
    log("debug", str)


def logInfo(str):
    log("info", str)


def logWarning(str):
    log("warn", str)


def logError(str):
    log("error", str)
