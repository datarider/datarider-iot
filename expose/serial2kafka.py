import re
import serial
from datetime import datetime
from kafka import KafkaProducer

localhost = "localhost:29092"
topic_name = "iot_dat_sensor_bronze"
port = "/dev/ttyACM0"
baud = 230400


# ===============================================================
#           MAIN
# ===============================================================


if __name__ == "__main__":

    # Init Kafka Producer
    producer = KafkaProducer(bootstrap_servers=localhost, api_version=(2, 13, 3))

    # connexion Arduino
    serial_port = serial.Serial(port=port, baudrate=baud)
    serial_port.flushInput()
    serial_port.flushOutput()

    # Kafka
    while True:
        if serial_port.inWaiting() > 0:
            data = serial_port.readline()
            print(data)
            # producer.send(topic_name, data)
            data = (
                str(data)
                .replace("b", "")
                .replace("r", "")
                .replace("n", "")
                .replace("'", "")
                .replace("\\", "")
            )

            try:
                (
                    track1_turn,
                    track1_gate_current,
                    track1_gate_active,
                    track1_negative_terminal,
                    track2_turn,
                    track2_gate_current,
                    track2_gate_active,
                    track2_negative_terminal,
                ) = str(data).split(";")

                turn_pattern = r"^\d{1,2}$"
                gate_pattern = r"^\d{1,3}$"
                negative_terminal_pattern = r"^-?\d{1,2}\.\d{1,2}$"

                turn_pattern_ok = bool(re.match(turn_pattern, track1_turn)) and bool(
                    re.match(turn_pattern, track2_turn)
                )

                gate_pattern_ok = (
                    bool(re.match(gate_pattern, track1_gate_current))
                    and bool(re.match(gate_pattern, track2_gate_current))
                    and bool(re.match(gate_pattern, track1_gate_active))
                    and bool(re.match(gate_pattern, track2_gate_active))
                )

                negative_terminals_pattern_ok = bool(
                    re.match(negative_terminal_pattern, track1_negative_terminal)
                ) and bool(
                    re.match(negative_terminal_pattern, track2_negative_terminal)
                )

                if (
                    turn_pattern_ok
                    and gate_pattern_ok
                    and negative_terminals_pattern_ok
                ):
                    ts = datetime.now().__str__()

                    data = (
                        ts
                        + ";"
                        + track1_turn
                        + ";"
                        + track1_gate_current
                        + ";"
                        + track1_gate_active
                        + ";"
                        + track1_negative_terminal
                        + ";"
                        + track2_turn
                        + ";"
                        + track2_gate_current
                        + ";"
                        + track2_gate_active
                        + ";"
                        + track2_negative_terminal
                    ).encode("utf-8")
                    print(data)
                    producer.send(topic_name, data)

            except Exception as inst:
                print(f"[ERROR] An exception occurred : {inst}")
            except KeyboardInterrupt:
                print(
                    "[ERROR] An exception occurred : Keyboard Interruption of this program"
                )
