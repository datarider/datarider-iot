#include <ACS712.h>

ACS712  ACS1(A0, 5.0, 1024, 100);
ACS712  ACS2(A1, 5.0, 1024, 100);

String message = "";
float a = 0.0;
// Init sensor var
int track1_turn = 0;
int track2_turn = 0;
String track1_gate_current = "255";
String track2_gate_current = "255";
String track1_previous_gate_current = "255";
String track2_previous_gate_current = "255";
String track1_gate_active = track1_gate_current;
String track2_gate_active = track2_gate_current;
float track1_negative_terminal = 0.0;
float track2_negative_terminal = 0.0;


// Floats for resistor values in divider (in ohms)
float R1 = 30000.0;
float R2 = 7500.0;
 // Float for Reference Voltage
float ref_voltage = 5.0;


void setup() {
  Serial.begin(230400);
  // Entrées circuit 1
  // circuit 1 = circuit extérieur

  pinMode(22, INPUT_PULLUP);
  pinMode(23, INPUT_PULLUP);
  pinMode(24, INPUT_PULLUP);
  pinMode(25, INPUT_PULLUP);
  pinMode(26, INPUT_PULLUP);
  pinMode(27, INPUT_PULLUP);
  pinMode(28, INPUT_PULLUP);
  pinMode(29, INPUT_PULLUP);

  // Entrées circuit 2
  // circuit 2 = circuit intérieur
  pinMode(30, INPUT_PULLUP);
  pinMode(31, INPUT_PULLUP);
  pinMode(32, INPUT_PULLUP);
  pinMode(33, INPUT_PULLUP);
  pinMode(34, INPUT_PULLUP);
  pinMode(35, INPUT_PULLUP);
  pinMode(36, INPUT_PULLUP);
  pinMode(37, INPUT_PULLUP);

  pinMode(A0, INPUT);
  Serial.flush();

}

void loop() {
    // Get negative_terminal from each track
    track1_negative_terminal = (analogRead(A3) * ref_voltage) / 1024.0 / (R2/(R1+R2));
    track2_negative_terminal = (analogRead(A2) * ref_voltage) / 1024.0 / (R2/(R1+R2));

    // Get current gate value
    track1_gate_current = String(PINA);
    track2_gate_current = String(PINC);

    // Get active gate value -- track_1
    if (track1_gate_current != "255")
    {
        track1_gate_active = track1_gate_current;
    }
    // Increment turn num -- track_1
    else
    {
        if (track1_previous_gate_current  == "254")
        {
              track1_turn = track1_turn + 1;
              if (track1_turn == 100) {track1_turn = 0;}
        }
    }
    track1_previous_gate_current = track1_gate_current;

    // Get active gate value -- track_2
    if (track2_gate_current != "255")
    {
        track2_gate_active = track2_gate_current;
    }
    // Increment turn num -- track_2
    else
    {
        if (track2_previous_gate_current  == "254")
        {
             track2_turn = track2_turn + 1;
             if (track2_turn == 100) {track2_turn = 0;}
        }
    }
    track2_previous_gate_current = track2_gate_current;


    // Generate message to send
    message = String(track1_turn) + String(";") + track1_gate_current + String(";") + track1_gate_active + String(";") + String(track1_negative_terminal) + String(";") + String(track2_turn) + String(";") +  track2_gate_current + String(";") + track2_gate_active + String(";") + String(track2_negative_terminal) ;

    // Send to computer the new message
    Serial.println(message);
}
