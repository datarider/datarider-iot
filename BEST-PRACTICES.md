# Data Rider - IOT - Best Practices

## Indentation
Always use the right indentation to facilitate code reading.

## Naming convention

### Format of variables and function names
For Data Rider, we choose to write in **Camel Case**.

### Meaningful names
Use simple naming, meaning: easily understood, easy to search and find, describes the role of the variable in your story.

## Separate actions into functions
Keep your function small.
Each function does one thing, and is the only place in the code that is in charge of that thing.

## Reusable
Whenever you want to perform a sequence of actions more than once, or if you have two similar classes/functions with only slight differences, consider to wrap the common part in a different class/function and then call it.

## Pick up the trash
It is sometimes tempting to keep every piece of code for “just-in-case”, but you have to let it go and get into a minimalist mindset when it comes to coding.

## Proper comments
Comments should add information, not repeat information in a different way.
Keep comments only for the parts that can't be explained by the code (e.g. explaining WHY you chose a specific way to do something, what else you tried and didn't work, etc’).

## Sources
- https://blog.wokwi.com/how-to-write-clean-arduino-code/
- https://docs.arduino.cc/learn/contributions/arduino-library-style-guide/
- https://github.com/Irikos/arduino-style-guide
